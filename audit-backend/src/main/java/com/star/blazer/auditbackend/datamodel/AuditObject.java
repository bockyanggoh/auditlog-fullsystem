package com.star.blazer.auditbackend.datamodel;

import com.google.gson.Gson;
import com.star.blazer.auditbackend.config.EnumList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("audit_store")
public class AuditObject {

    private @Id String id;
    private @Indexed Date timestamp;
    private @Indexed EnumList.ACTIONLIST action;
    private @Indexed String username;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}


