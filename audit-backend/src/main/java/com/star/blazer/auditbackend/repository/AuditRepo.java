package com.star.blazer.auditbackend.repository;

import com.star.blazer.auditbackend.datamodel.AuditObject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditRepo extends CrudRepository<AuditObject, String> {

    List<AuditObject> getAll();

    List<AuditObject> getAllByAction_Click();

    List<AuditObject> getAllByAction_Login();

    List<AuditObject> getAllByUsernameEquals(String username);

    List<AuditObject> getAllByUsernameAndActionEquals(String username, String action);

}
